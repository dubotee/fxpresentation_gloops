﻿// First Fantasy for Mobile version: 1.3.2
// Author: Gold Experience Team (http://www.ge-team.com/)
// Support: geteamdev@gmail.com
// Please direct any bugs/comments/suggestions to geteamdev@gmail.com

#region Namespaces

using UnityEngine;
using System.Collections;

#endregion

/***************
* FFController class.
* 
* 	This class handles
* 		- Switch Camera and Player control
* 		- Init Render Setting
* 		- Display GUIs
* 
***************/

public class TestbedController : MonoBehaviour
{

#region Variables
	public Material m_SkyBoxMaterial = null;
	public GameObject m_OrbitCamera = null;

	GameObject m_Rain = null;
	GameObject m_Fog = null;
	GameObject m_Snow = null;
	GameObject m_Tornado = null;
	GameObject m_Sandstorm = null;
	GameObject m_Torchfire = null;
	GameObject m_Clouds = null;

	public GameObject m_ExplosionPrefab = null;
	
#endregion {Variables}
	
// ######################################################################
// MonoBehaviour Functions
// ######################################################################

#region Component Segments

	// Use this for initialization
	void Start ()
	{
		InitCamera();

		m_Rain = GameObject.Find ("Rain");
		m_Fog = GameObject.Find ("Fog");
		m_Snow = GameObject.Find ("Snow");
		m_Tornado = GameObject.Find ("Tornado");
		m_Sandstorm = GameObject.Find ("Sandstorm");
		m_Torchfire = GameObject.Find ("Torchfire");
		m_Clouds = GameObject.Find ("Clouds");

		m_Rain.GetComponent<ParticleSystem> ().Stop ();
		m_Fog.GetComponent<ParticleSystem> ().Stop ();
		m_Snow.GetComponent<ParticleSystem> ().Stop ();
		m_Tornado.GetComponent<ParticleSystem> ().Stop ();
		m_Sandstorm.GetComponent<ParticleSystem> ().Stop ();
		m_Torchfire.GetComponent<ParticleSystem> ().Stop ();
		
		m_Clouds.SetActive (false);
	}
	
	// Update is called once per frame
	void Update ()
	{
	}
	
	void OnTriggerExit(Collider other)
	{		
		// Reset player position when user move it away from terrain
		this.transform.localPosition = new Vector3(0,1,0);
    }
	
	// OnGUI is called for rendering and handling GUI events.
	void OnGUI () {
		// Show Scenes window
		GUI.Window(3, new Rect(5, Screen.height-110, 340, 105), DemoScenesWindow, "Demo Scenes");
		
	}
	
#endregion Component Segments
	
// ######################################################################
// Functions Functions
// ######################################################################

#region Functions
	
	void InitCamera()
	{
		m_OrbitCamera.SetActive(true);
	}

	void SwitchCamera()
	{
		m_OrbitCamera.SetActive(!m_OrbitCamera.activeSelf);
		
		if(m_OrbitCamera.activeSelf==true)
		{
			FFOrbitCamera pFFOrbitCamera = (FFOrbitCamera) Object.FindObjectOfType(typeof(FFOrbitCamera));
			pFFOrbitCamera.TargetLookAt.transform.localPosition = new Vector3(0,0,0);
		}
	}

	// Show Demo Scenes window
	void DemoScenesWindow(int id)
	{
		string[] DemoScenesName = {"1) Rain", "2) Snow", "3) Tornado on/off", "4) Add Explosive", "5) Torchfire on/off", "6) On/Off Clouds", "7) Reset"};
		GUIStyle buttonStyle = GUI.skin.GetStyle("Button");
		buttonStyle.alignment = TextAnchor.MiddleLeft;
		int columnCount = 0;
		for(int i=0;i<DemoScenesName.Length;i++)
		{
			// Disable current scene load button
			if(Application.loadedLevelName == DemoScenesName[i])
			{
				GUI.enabled=false;
			}
			if(GUI.Button(new Rect(10 + (110*columnCount), 25*((i/3)+1), 100, 20), DemoScenesName[i], buttonStyle))
			{
				if (i == 0)
				{
					m_Rain.GetComponent<ParticleSystem> ().Play ();
					m_Fog.GetComponent<ParticleSystem> ().Stop ();
					m_Snow.GetComponent<ParticleSystem> ().Stop ();
					m_Tornado.GetComponent<ParticleSystem> ().Stop ();
					m_Sandstorm.GetComponent<ParticleSystem> ().Stop ();
				}
				else if (i == 1)
				{
					m_Rain.GetComponent<ParticleSystem> ().Stop ();
					m_Fog.GetComponent<ParticleSystem> ().Play ();
					m_Snow.GetComponent<ParticleSystem> ().Play ();
					m_Tornado.GetComponent<ParticleSystem> ().Stop ();
					m_Sandstorm.GetComponent<ParticleSystem> ().Stop ();
				}
				else if (i == 2)
				{
					if (m_Tornado.GetComponent<ParticleSystem>().isPlaying)
					{
						m_Tornado.GetComponent<ParticleSystem> ().Stop ();
						m_Sandstorm.GetComponent<ParticleSystem> ().Stop ();
					}
					else
					{
						m_Tornado.GetComponent<ParticleSystem> ().Play ();
						m_Sandstorm.GetComponent<ParticleSystem> ().Play ();
					}
				}
				else if (i == 3)
				{
					Instantiate(this.m_ExplosionPrefab, new Vector3(Random.Range(3.0f,-3.0f), -0.12f, 5.0f), Quaternion.identity);
				}
				else if (i == 4)
				{
					if ( m_Torchfire.GetComponent<ParticleSystem> ().isPlaying )
					{
						m_Torchfire.GetComponent<ParticleSystem> ().Stop ();
					}
					else
					{
						m_Torchfire.GetComponent<ParticleSystem> ().Play ();
					}
				}
				else if (i == 5)
				{
					m_Clouds.SetActive (!m_Clouds.activeSelf);
				}
				else if (i == 6)
				{
					m_Rain.GetComponent<ParticleSystem> ().Stop ();
					m_Fog.GetComponent<ParticleSystem> ().Stop ();
					m_Snow.GetComponent<ParticleSystem> ().Stop ();
					m_Tornado.GetComponent<ParticleSystem> ().Stop ();
					m_Sandstorm.GetComponent<ParticleSystem> ().Stop ();
					m_Torchfire.GetComponent<ParticleSystem> ().Stop ();
				}
			}
			if(GUI.enabled==false)
			{
				GUI.enabled=true;
			}

			// reset column
			columnCount++;
			if(columnCount==3)
			{
				columnCount = 0;
			}
		}
	}

#endregion Functions
	
}
