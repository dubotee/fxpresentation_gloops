﻿using UnityEngine;
using System.Collections;

public class SnowParticleListener : MonoBehaviour {
	int snowThickness = 0;
	GameObject m_Snow = null;

	// Use this for initialization
	void Start () {
		Terrain t = this.GetComponent<Terrain>();
		t.materialTemplate.SetFloat("_LayerStrength", 0.0f);
		m_Snow = GameObject.Find ("Snow");
	}
	
	// Update is called once per frame
	void Update () {
		snowThickness=snowThickness<=0?0:--snowThickness;
		Terrain t = this.GetComponent<Terrain>();
		t.materialTemplate.SetFloat("_LayerStrength", snowThickness/500.0f);
	}

	void OnParticleCollision(GameObject other) {
		if(other == m_Snow)
		{
			snowThickness = snowThickness/500.0f > 1 ? snowThickness : snowThickness + 3;
		}
	}
}
